# Apache2 HTML Server (Raspberry Pi) / Dev Portfolio (HTML/CSS/NodeJS/ExpressJS)

# What I learned:
- A basic introduction to TCP/UDP Protocols and establishing links between the network and pi.
- An introduction to the Raspberry Pi and some of its capabilities
- An introduction to creating a website as a NodeJS project using Express.

http://briansukhnandan.com/ - To access my Website Portfolio